This wiki contains the public documentation of the Tails Sysadmin team that is
still valid. This documentation will be gradually superseded by the TPA doc
during [the merge
process](/tpo/tpa/team/-/wikis/policy/tpa-rfc-73-tails-infra-merge-roadmap).

These is the content that still lives here for now:

- [[how-tos|tails/how-tos]]
- [[processes|tails/processes]]
- [[role-description|tails/role-description]]
- [[services|tails/services]]
- [[scripts|tails/scripts]]

Note: this wiki also contains non-markdown file, clone the corresponding repo
to see them.
