---
title: TPA-RFC-81: Gitlab access
costs: staff
approval: TPO engineering team leads
affected users: engineering teams
status: approved
---

Summary: adopt a gitlab access policy to regulate roles, permissions and access to repositories in https://gitlab.torproject.org

# Background

The Tor Project migrated from Trac (bug tracker) into its own Gitlab Instance in 2020. We migrated all users from Trac into Gitlab and disabled the ones that were not used. For Tor Project to use Gitlab we mirrored the teams structure we have in the organization. There is a main "TPO Group" that contains all sub-groups. Each sub-group is a team at the Tor project. We also created an 'organization' project that hosts the main wiki from Tor. We started adding people from each team to their group in Gitlab. The main "TPO" group gets full access from the executive director, the project managers, the director of engineers, the community team lead and the network product manager. But there has not been an official policy to regulates who should have access, who controls who has access and how we go about approving that access. This policy is a first attempt to write down that Gitlab access policy.  This policy has only been approved by engineering teams and only affects the groups ux, core, network-health, anti-censorship and applications in Gitlab. 

# Proposal

These guidelines outline best practices for managing access to GitLab projects and groups within our organization. They help ensure proper handling of permissions, secure access to projects, and adherence to our internal security standards, while allowing flexibility for exceptions as needed.

These guidelines follow the Principle of Least Privilege: All users should only be granted the minimum level of access necessary to perform their roles. Team leads and GitLab administrators should regularly assess access levels to ensure adherence to this principle.

1. Group Membership and Access Control

Each team lead is generally responsible for managing the membership and access levels within their team's GitLab group. They should ensure that team members have appropriate permissions based on their roles.

Default Group Membership: Typically, team members are added to their team’s top-level group, inheriting access to all projects under that group, based on their assigned roles (e.g., Developer, Maintainer).

Exceptions: In some cases, there are users who are not team members but require access to the entire group. These instances are exceptions and should be carefully evaluated. When justified, these users can be granted group-level access, but this should be handled cautiously to prevent unnecessary access to other projects. These cases are important to be included in a regular audit so that this broad project level access is not maintained after such a person is no longer involved.

**2FA Requirement for Group Access**: All users with access to a team's Gitlab group, including those granted exceptional group-level access, must have two-factor authentication (2FA) enabled on their Gitlab account. This applies to both employees and external collaborators who are granted access to the group. Team leads are responsible for ensuring that users with group-level access have 2FA enabled. 

2. Limiting Project-Specific Access

If a user requires access to a specific project within a team's group, they should be added directly to that project instead of the team’s group. This ensures they only access the intended project and do not inherit access to other projects unnecessarily.

3. Handling Sensitive Projects

For projects requiring more privacy or heightened security, GitLab administrators may create a separate top-level group outside the main team group. These groups can be made private, with access being tightly controlled to fit specific security needs. This option should be considered for projects involving sensitive data or security concerns.

4. Periodic Access Reviews

Team leads should periodically review group memberships and project-specific access levels to ensure compliance with these guidelines. Any discrepancies, such as over-privileged access, should be corrected promptly.

During periodic access reviews, compliance with the 2FA requirement should be verified. Any users found without 2FA enabled should have their access revoked until they comply with this requirement.

5. Dispute Resolution

In cases where access disputes arise (e.g., a user being denied access to a project or concerns over excessive permissions), the team lead should first attempt to resolve the issue directly with the user.

If the issue cannot be resolved at the team level, it should be escalated to include input from relevant stakeholders (team leads, project managers, GitLab administrators). A documented resolution should be reached, especially if the decision impacts other team members or future access requests.

## Affected users

All Tor Project's Gitlab users and Tor's community in general.

# Approvals required

This proposal has been approved by engineering team leads. The engineering teams at TPI are network team, network health team, anti-censorship team, ux team and applications team.
