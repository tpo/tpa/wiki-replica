---
title: TPA-RFC-77: Move Tails Sysadmins issue trackers to Tor's GitLab
costs: None
approval: TPA
affected users: TPA, Tails team
deadline: October 4th 2024
status: accepted
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41753
---

[[_TOC_]]

Summary: move the Tails sysadmin issues from Tails' Gitlab to Tor's Gitlab.

# Background

With the merge between Tails and Tor, Tails sysadmins are now part of TPA. Issues for the Tails sysadmins are now spread across two different Gitlab instances. This is quite a nuisance for triaging and roadmapping.

# Proposal

This proposal aims to migrate the tails/sysadmin and tails/sysadmin-private projects from Tails' Gitlab instance to Tor's Gitlab instance in the tpo/tpa/tails namespace.

To preserve authorship, users who have created, were assigned to, or commented on issues in these projects will be migrated as well. Users who have not contributed anything for more than a year will be deactivated on Tor's Gitlab instance.

## Goals

The goal is to have all sysadmin issues on one single Gitlab instance.

### Must have

  - all sysadmin issues in one Gitlab instance
  - preserved authorship
  - preserved labels

### Nice to have

  - redirection of affected Tails' Gitlab projects to Tor's Gitlab

### Non-Goals

  - migrating code repositories

## Scope

The migration concerns the tails/sysadmin project, the tails/sysadmin-private project, and all users who created, were assigned to, or commented on issues in these projects. The rest of the Tails Gitlab, including any sysadmin owned code repositories, are out of scope for this proposal.

## Plan

- [ ] Wait for the merge to go public
- [ ] Freeze tails/sysadmin on Tails' Gitlab
- [ ] As root on Tails' Gitlab, make an export of the tails/sysadmin project
- [ ] Wait for the export to complete, download it, and unpack the tgz
- [ ] Archive the tails/sysadmin project on Tails' Gitlab
- [ ] Retrieve all the user id's that have been active in the project issues:
      ``cat tree/project/issues.ndjson |jq ''|grep author_id|sed -e 's/^ *"author_id": //'|sed -e 's/,//' |sort |uniq > uids.txt``
- [ ] for each uid:
  - check if their username and/or email exists in tor's gitlab
  - if only one of the two exists or both exist but they do not match:
    - contact the user and ask how they want to resolve this
    - proceed accordingly
  - if both exist and match:
    - add the user details to tree/project/project_members.ndjson
    - use tails' user_id and set their public_email attribute
    - set access_level to 10 (guest)
  - if they do not exist:
    - create an account for them on tor's gitlab
    - check if they had recent activity on tails' gitlab, if so:
      - send them an email explaining the merge and providing activation instructions
    - else:
      - block their account
    - add the user details to tree/project/project_members.ndjson
    - use tails' user_id and set their public_email attribute
    - set access_level to 10 (guest)
- [ ] tar and gzip the export again
- [ ] On Tor's Gitlab, enable imports from other gitlab instances
- [ ] On Tor's Gitlab, create the tails/sysadmin project by importing the new tgz file
- [ ] On Tor's Gitlab, move the tails/sysadmin project to tpo/tpa/tails/sysadmin
- [ ] Raise access levels as needed
- [ ] Repeat for sysadmin-private

Once migrated, ask immerda to redirect gitlab.tails.boum.org/tails/sysadmin to gitlab.torproject.org/tails/sysadmin , ditto for sysadmin-private.

Finally, edit all wikis and code repositories that link to the sysadmin project as issue tracker and replace with the Tor Gitlab.

## Timeline

The migration should be performed in one day, as soon as the RFC is approved (ideally in the second week of October).

## Affected users

This proposal primarily affects TPA and the Tails team. To a lesser degree Tails contributors who have interacted with sysadmins in the past are affected as they will receive accounts on Tor's Gitlab.

The main technical implication of this migration is that it will no longer be possible to link directly between tails issues and sysadmin issues. This will be resolved if/when the rest of the Tails Gitlab is migrated to Tor's Gitlab.
