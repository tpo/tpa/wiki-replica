---
title: TPA-RFC-19: GitLab Labels usage
costs: staff
approval: TPA
affected users: GitLab users interacting with TPA projects and service admins
deadline: One week, starting at 2022-03-16, so 2022-03-23.
status: standard
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40649
---

Summary: create a bunch of labels or projects to regroup issues for
all documented services, clarify policy on labels (mostly TPA
services) vs projects (git, external consultants) usage.

[[_TOC_]]

# Background

Inside TPA, we have used, rather inconsistently, projects for certain
things (e.g. `tpo/tpa/gitlab`) and labels for others
(e.g. ~Nextcloud). It's unclear when to use which and why. There's
also a significant number of services that don't have any project or
label associated with them.

This proposal should clarify this.

## Goals

### Must have

 * we should know whether we should use a label or project when
   reporting a bug or creating a service

[service list]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/service

### Nice to have

 * every documented service in the [service list][] should have a
   label or project associated with it

# Proposal

Use a project when:

 * the service has a git repository
   (e.g. `tpo/tpa/dangerzone-webdav-processor`, most web sites)
 * the service is *primarily* managed by service admins
   (e.g. `tpo/tpa/schleuder`) or external consultants
   (e.g. `tpo/web/civicrm`) who are actively involved in the GitLab
   server and issue queues

Use a label when:

 * the service is *only* (~DNS) or *primarily* (~Gitlab) managed by
   TPA
 * it is not a service (e.g. ~RFC)

## Scope

This applies only to TPA services and services managed by "service
admins".

## Current labels

TODO: should we add an "issues" column to the service list with this data?

Those are the labels that are currently in use inside `tpo/tpa/team`:

| Label          | Fate    | Note                                            |
|----------------|---------|-------------------------------------------------|
| ~Cache         | keep    | deprecated, but shouldn't be removed            |
| ~DNS           | keep    | need reference in doc                           |
| ~Deb.tpo       | keep    |                                                 |
| ~Dist          | keep    |                                                 |
| ~Email         | keep    | needs documentation page!                       |
| ~Git           | keep    |                                                 |
| ~Gitlab        | keep    |                                                 |
| ~Gitweb        | keep    |                                                 |
| ~Jenkins       | keep    |                                                 |
| ~LDAP          | keep    |                                                 |
| ~Lists         | keep    |                                                 |
| ~Nextcloud     | move    | external service, move to project               |
| ~RFC           | keep    |                                                 |
| ~RT            | keep    |                                                 |
| ~Schleuder     | move?   | move issues to existing project                 |
| ~Service admin | remove? | move issues to other project/labels             |
| ~Sysadmin      | remove  | everything is sysadmin, clarify                 |
| ~incident      | keep    | internally used by GitLab for incident tracking |

## New labels

Those are labels that would need to be created inside `tpo/tpa/team`
and linked in their service page.

| Label             | Description                       | Note                                                |
|-------------------|-----------------------------------|-----------------------------------------------------|
| ~Backup           | backup services                   |                                                     |
| ~BBB              | Video and audio conference system | external consultants not on GitLab                  |
| ~BTCpayserver     | TBD                               | TODO: is that a TPA service now?                    |
| ~CI               | issues with GitLab runners, CI    |                                                     |
| ~DRBD             |                                   | is that really a service?                           |
| ~Ganeti           |                                   |                                                     |
| ~Grafana          |                                   |                                                     |
| ~IRC              |                                   | TODO: should that be external?                      |
| ~IPsec            |                                   |                                                     |
| ~kvm              |                                   | deprecated, don't create?                           |
| ~Logging          | centralized logging server        | maybe expand to include all logging and PII issues? |
| ~Nagios           | Nagios/Icinga monitoring server   | rename to Icinga?                                   |
| ~Openstack        | Openstack deployments             |                                                     |
| ~PostgreSQL       | PostgreSQL database services      |                                                     |
| ~Prometheus       |                                   |                                                     |
| ~Puppet           |                                   |                                                     |
| ~static-component |                                   |                                                     |
| ~static-shim      | static site / GitLab shim         |                                                     |
| ~SVN              |                                   |                                                     |
| ~TLS              | X509 certificate management       |                                                     |
| ~WKD              | OpenPGP certificates distribution |                                                     |

Note that undocumented and retired projects do *not* currently have
explicit labels or projects associated with them.

## Current projects

Those are services which currently have a project associated with them:

| Service     | Project                               | Fate   | Note                                                   |
|-------------|---------------------------------------|--------|--------------------------------------------------------|
| GitLab      | tpo/tpa/gitlab                        | retire | primarily maintained by TPA move all issues to ~Gitlab |
| status      | tpo/tpa/status-site                   | keep   | git repository                                         |
| blog        | tpo/web/blog                          | keep   | git repository                                         |
| bridgedb    | ?                                     | ?      | anti-censorship team                                   |
| bridgestrap | ?                                     | ?      | idem                                                   |
| check       | ?                                     | ?      | network health team?                                   |
| CRM         | `tpo/web/civicrm`                     | keep   | external consultants                                   |
| collector   | ?                                     | ?      | network health team                                    |
| dangerzone  | `tpo/tpa/dangerzone-webdav-processor` | keep   | git repository                                         |
| metrics     | ?                                     | ?      | metrics team                                           |
| moat        | ?                                     | ?      | anti-censorship                                        |
| newsletter  | `tpo/web/newsletter`                  | keep   | git repository                                         |
| onionperf   | ?                                     | ?      | metrics team                                           |
| schleuder   | tpo/tpa/schleuder                     | keep   | schleuder service admins?                              |
| rdsys       | ?                                     | ?      | anti-censorship team                                   |
| snowflake   | ?                                     | ?      | idem                                                   |
| styleguide  | `tpo/web/styleguide`                  | keep   | git repository                                         |
| support     | `tpo/web/support`                     | keep   | git repository                                         |
| survey      | ???                                   | ???    | ???                                                    |
| website     | tpo/web/tpo                           | keep   | git repository                                         |

### New projects

Those are services that should have a new project created for them:

| Project             | Description | Note                               |
|---------------------|-------------|------------------------------------|
| `tpo/tpa/nextcloud` |             | to allow Riseup to manage tickets? |

# Personas

## Anathema: the sysadmin

Anathema manages everything from the screws on the servers to the CSS
on the websites. Hands in everything,
jake-of-all-trades-master-of-none, that's her name. She is a GitLab
admin, but normally uses GitLab like everyone else. She files a
boatload of tickets, all over the place. Anathema often does triage
before the triage star of the week even wakes up in the morning.

Changes here won't change her work much: she'll need to remember to
assign issues to the right label, and will have to do a bunch of
documentation changes if that proposal passes.

## Wouter: the webmaster

Wouter works on many websites and knows Lektor inside and out. He
doesn't do issues much except when he gets beat over the head by the
PM to give estimates, arghl.

Changes here will not affect his work: his issues will mostly stay in
his project, because most of them already have a Git repository
assigned.

## Petunia: the project manager

Petunia has a global view of all the projects at Tor. She's a GitLab
admin and her mind holds more tickets in her head than you ever will
even imagine.

Changes here will not affect her much because she already has a global
view. She should be able to help move tickets around and label
everything properly after the switch.

## Charlie, the external consultant

Charlie was hired to deal with CiviCRM but also deals with the
websites.

Their work won't change much because all of those services already
have projects associated.

## Mike, the service provider

Mike provides us with our Nextcloud service, and he's awesome. He can
debug storage problem while hanging by his toes off a (cam)bridge
while simultaneously fighting off DDOS attacks from neonazi trolls.

He typically can't handle the Nextcloud tickets because they are often
confidential, which is annoying. He has a GitLab account so he will be
possibly happy to be able to do triage in a new Nextcloud project, and
see confidential issues there. He will also be able to watch those
issues specifically.

## George, the GitLab service admin

George is really busy with dev work, but really wanted to get GitLab
off the ground so they helped with deploying GitLab, and now they're
kind of stuck with it. They helped an intern develop code for
anonymous tickets, and triaged issues there. They also know a lot
about GitLab CI and try to help where they can.

Closing down the GitLab subproject means they won't be able to do
triage unless they are added to the TPA team, something TPA has been
secretly conspiring towards for months now, but that, no way in hell,
will not happen.

# Alternatives considered

## All projects

In this approach, all services would have a project associated with
them. In [issue tpo/tpa/gitlab#10][], we considered that approach,
arguing that there were too many labels to chose from so it was hard
to figure out which one to pick. It was also argued that users can't
pick labels so that we'd have to do the triage anyways. And it is true
that we do not necessarily assign the labels correctly right now.

Ultimately, however, having a single project to see TPA-specific
issues turned out to be critical to survive the onslaught of tickets
in projects like [GitLab lobby](https://gitlab.torproject.org/tpo/tpa/gitlab-lobby/), [Anon ticket](https://gitlab.torproject.org/tpo/tpa/anon_ticket) and others. If
every single service had its own project, it would mean we'd have to
triage *all* those issues at once, which is currently overwhelming.

[issue tpo/tpa/gitlab#10]: https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/10

## All labels

In this approach, all services would be labels. This is simply not
possible, if only because some service absolutely *do* require a
separate project to host their git repository.

## Both project and label

We could also just have a label *and* a project, e.g. keep the status
quo between `tpo/tpa/gitlab` and ~Gitlab. But then we can't really
tell where to file issues, and even less where to see the whole list
of issues.

# References

This proposal is discussed in [issue tpo/tpa/team#40649][]. Previous
discussion include:

 * [issue tpo/tpa/gitlab#10][], "move tpa issues into subprojects or
   cleanup labels"; ended up in the status quo: current labels kept,
   no new subproject created
 * [issue tpo/tpa/gitlab#55][], "move gitlab project back into
   tpo/tpa/team"; ended up deciding to keep the project and create
   subprojects for everything (ie. reopening tpo/tpa/gitlab#10 above,
   which was ultimately closed)

[issue tpo/tpa/gitlab#55]: https://gitlab.torproject.org/tpo/tpa/gitlab/-/issues/55
[issue tpo/tpa/team#40649]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/40649

See also the [TPA-RFC-5: GitLab migration](policy/tpa-rfc-5-gitlab) proposal which sets the
policy on other labels like ~Doing, ~Next, ~Backlog, ~Icebox and so on.
