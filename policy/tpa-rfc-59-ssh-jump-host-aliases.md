---
title: TPA-RFC-59: SSH jump host aliases
status: standard
discussion: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41351
---

[[_TOC_]]

Summary: new aliases were introduced to use as jump hosts, please
start using `ssh.torproject.org`, `ssh-dal.torproject.org`, or
`ssh-fsn.torproject.org`, depending on your location.

# Background

Since time immemorial, TPA has restricted SSH access to an allow list
of servers for all servers. A handful of servers had an exception for
this, and those could be used to connect or "jump" to the other hosts,
with the `ssh -J` command-line flag or the `ProxyJump` SSH
configuration option.

Traditionally, the `people.torproject.org` host has been used for this
purpose, although this is just a convention.

# Proposal

New aliases have been introduced:

 * `ssh-dal.torproject.org` - in [Dallas, TX, USA][]
 * `ssh-fsn.torproject.org` - in [Falkenstein, Saxony, Germany][],
   that is currently provided by `perdulce`, also known as
   `people.torproject.org`, but this could change in the future
 * `ssh.torproject.org` - alias for `ssh-dal`, but that will survive
   any data center migration

You should be able to use those new aliases as a more reliable way to
control latency when connecting over SSH to your favorite hosts. You
might want, for example, to use the `ssh-dal` jump host for machines
in the `gnt-dal` cluster, as the path to those machines will be
shorter (even if the first hop is longer).

We unfortunately do not have a public listing of where each machine is
hosted, but when you log into a server, you should see where it is,
for example, the `/etc/motd` file shown during login on `chives` says:

     This virtual server runs on the physical host gnt-fsn.

You are welcome to use the `ping` command to determine the best
latency, including running `ping` on the jump host itself, although we
MAY actually remove shell access on the jump host themselves to
restrict access to only port forwarding.

[Dallas, TX, USA]: https://en.wikipedia.org/wiki/Dallas
[Falkenstein, Saxony, Germany]: https://en.wikipedia.org/wiki/Falkenstein,_Saxony

# Deadline

This is more an announcement than a proposal: the changes have already
been implemented. Your feedback on naming is still welcome and we will
take suggestions on correcting possible errors for another two weeks.

# References

Documentation on how to use jump hosts has been modified to include
this information, head to the [doc/ssh-jump-host][] for more
information.

Credits to @pierov for suggesting a second jump host, see
[tpo/tpa/team#41351][] where your comments are also welcome.

[doc/ssh-jump-host]: https://gitlab.torproject.org/tpo/tpa/team/-/wikis/doc/ssh-jump-host/
[tpo/tpa/team#41351]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/41351
