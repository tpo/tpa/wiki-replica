TODO: integrate this into template

TODO: this is not even a draft, turn this into something a human can read

# reading notes from the PSNA 3ed ch 22 (Disaster recovery)

 1. risk analysis
  - which disasters
  - what risk
  - cost = budget (B)
  
    B = (D-M)xR
    D: cost of disaster
    M: cost after mitigation
    R: risk

 8. plan for media response
    - who
    - what to say (and not to say)

 9. summary
    - find critical services
    - simple ways
    - automation (e.g. integrity checks)

## questions

 - which components to restore first?
 - how fast?
 - what is most likely to fail?

Also consider "RPO" (Recovery Point Objective, how much stuff can we
afford to lose, time wise) and "RTO" (Recovery Time Objective, how
long it will take to recover it). Wikipedia has a [good
introduction](https://en.wikipedia.org/wiki/Disaster_recovery) on this, and especially this diagram:

![schema showing the difference between RPO and RTO](https://upload.wikimedia.org/wikipedia/commons/6/69/RPO_RTO_example_converted.png)

# Establishing a DR plan for TPA/TPI/TPO (?)

 1. "send me your disasters" cf 22.1 (above)
 
    (ie. which risk & cost) + what to restore first, how fast?
    
 2. derive security policy from DR plan
 
    e.g.
    
    - check your PGP keys
    - FDE?
    - signal
    - password policy
    - git integrity
    - separate access keys (incl. puppet) for backups

# References

 * [discussion ticket](https://gitlab.torproject.org/tpo/tpa/team/-/issues/40628)
 * [security policy ticket](https://gitlab.torproject.org/tpo/team/-/issues/41)
