# User Documentation

This documentation is primarily aimed at users.

Note: most of this documentation is a little chaotic and needs to be
merged with the [service listing](service). You might interested in
one of the following quick links instead:

 * [Email](service/email)
 * [Forum (Discourse)](service/forum)
 * [GitLab](howto/gitlab)
 * [Nextcloud](service/nextcloud)
 * [IRC](howto/irc)
 * [Yubikeys](howto/yubikey)

Other documentation:

<!-- update with `ls -d doc/*.md | sed 's/.md$//;s/\(.*\)/ * [\1](doc\/\1)/'` -->

 * [accounts](doc/accounts)
 * [admins](doc/admins)
 * [bits-and-pieces](doc/bits-and-pieces)
 * [extra](doc/extra)
 * [hardware-requirements](doc/hardware-requirements)
 * [how-to-get-help](support)
 * [naming-scheme](doc/naming-scheme)
 * [reporting-email-problems](doc/reporting-email-problems)
 * [services](doc/services)
 * [ssh-jump-host](doc/ssh-jump-host)
 * [static-sites](doc/static-sites)
 * [svn-accounts](doc/svn-accounts)
