The Tor Project runs a public Debian package repository intended for the
distribution of Tor experimental packages built from CI pipelines in project
[tpo/core/tor/debian](https://gitlab.torproject.org/tpo/core/debian/tor).

The URL for this service is https://deb.torproject.org

[[_TOC_]]

# Tutorial

## How do I use packages from this repository?

See the tutorial instructions over at: https://support.torproject.org/apt/tor-deb-repo/

# How-to

## Cut a new release

 1. Ensure your PGP key is present in the project's `TOR_DEBIAN_RELEASE_KEYRING_DEBIAN` CI/CD file variable
 2. Create and PGP-sign a new tag on the `debian-<major>-<minor>` branch
 3. Push and wait for the CI pipeline run
 4. Promote the packages in reprepro:

   for i in $(list-suites | grep proposed-updates | grep -v tor-experimental); do echo " " reprepro -b /srv/deb.torproject.org/reprepro copysrc ${i#proposed-updates/} $i tor; done

 5. Run `static-update-component deb.torproject.org` to update the mirrors

## List all packages

The `show-all-packages` command will list packages hosted in the repository,
including information about the provided architectures:

    tordeb@palmeri:/srv/deb.torproject.org$ bin/show-all-packages

## Remove a package

```
tordeb@palmeri:/srv/deb.torproject.org$ bin/show-all-packages | grep $PACKAGETOREMOVE
tordeb@palmeri:/srv/deb.torproject.org$ reprepro -b /srv/deb.torproject.org/reprepro remove $RELEVANTSUITE $PACKCAGETOREMOVE
```

Packages are probably in more than one suite. Run `show-all-packages` again at the end to make sure you got them all.

## Add a new suite

 1. Enable building a source package for the suite in `debian/misc/build-tor-sources`
 2. Add the binary build job for the new suite in the job matrix in `debian/.debian-ci.yml`
 3. Add the suite in `/srv/deb.torproject.org/reprepro/conf/gen-suites` and run the script
 4. Ensure your PGP key is present in the project's `TOR_DEBIAN_RELEASE_KEYRING_DEBIAN` CI/CD file variable
 4. Merge the `debian-ci` branch and run a CI pipeline in the `tpo/core/debian/tor` project
 6. Run `show-all-packages` on `palmeri` to ensure the new package was added in `proposed-updates`
 7. Promote the packages in reprepro: `reprepro -b /srv/deb.torproject.org/reprepro copysrc <suite> proposed-updates/<suite> tor`
 8. Run `static-update-component deb.torproject.org` to update the mirrors

## Add a new architecture

 1. Add the architecture in the job matrix in `debian/.debian-ci.yml` (`debian-ci` branch)
 2. Add the architecture in `/srv/deb.torproject.org/reprepro/conf/gen-suites` and run the script
 3. Ensure your PGP key is present in the project's `TOR_DEBIAN_RELEASE_KEYRING_DEBIAN` CI/CD file variable
 4. Merge the `debian-ci` branch and run a CI pipeline in the `tpo/core/debian/tor` project
 5. Run `show-all-packages` on `palmeri` to ensure the new package was added in `proposed-updates`
 6. "Flood" the suites in reprepro to populate arch-all packages

    for i in $(list-suites | grep -Po "proposed-updates/\K.*" | grep -v tor-experimental); do echo " " reprepro -b /srv/deb.torproject.org/reprepro flood $i; done

 7. Run `static-update-component deb.torproject.org` to update the mirrors

# Reference

* Host: palmeri.torproject.org
* All the stuff: `/srv/deb.torproject.org`
* LDAP group: tordeb

The repository is managed using
[reprepro](https://wikitech.wikimedia.org/wiki/Reprepro).

The primary purpose of this repository is to provide a repository with
experimental and nightly tor packages. Additionally, it provides up-to-date
backports for Debian and Ubuntu suites.

Some backports have been maintained here for other packages, though it is
preferred that this happens in Debian proper. Packages that are not at least
available in Debian testing will not be considered for inclusion in this
repository.

## Design

## Maintainer, users, and upstream

## Packages

The following packages are available in the repository:

### deb.torproject.org-keyring

* Maintainer: weasel
* Suites: all regular non-experimental suites

It contains the archive signing key.

### tor

* Maintainer: weasel
* Suites: all regular suites, including experimental suites

Builds two binary packages: tor and tor-geoipdb.

# Discussion

## Other alternatives

You do not need to use deb.torproject.org to be able to make Debian packages
available for installation using apt! You could instead host a Debian
repository in your people.torproject.org webspace, or alongside releases at
dist.torproject.org.
