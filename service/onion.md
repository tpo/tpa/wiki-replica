Onion services are the `.onion` addresses of services hosted by TPA,
otherwise accessible under `.torproject.org`.

This service is gravely undocumented.

[[_TOC_]]

# Tutorial

<!-- simple, brainless step-by-step instructions requiring little or -->
<!-- no technical background -->

# How-to

<!-- more in-depth procedure that may require interpretation -->

## Pager playbook

### Descriptor unreachable

The `OnionProbeUnreachableDescriptor` looks like:

    Onion service unreachable: eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion

It means the onion service in question (the lovely
`eweiibe6tdjsdprb4px6rqrzzcsi22m4koia44kc5pcjr7nec2rlxyad.onion`) is
currently inaccessible by the onion monitoring service,
[onionprobe](https://gitlab.torproject.org/tpo/onion-services/onionprobe).

Typically, it means users accessing the onion service are unable to
access the service. It's an outage that should be resolved, but it
only affects users accessing the service over Tor, not necessarily
other users.

You can confirm the issue by visiting the URL in Tor Browser.

We are currently aware of issues with onion services, see
[tpo/tpa/team#42054](https://gitlab.torproject.org/tpo/tpa/team/-/issues/42054) and [tpo/tpa/team#42057](https://gitlab.torproject.org/tpo/tpa/team/-/issues/42057). Typically, the
short-term fix is to restart Tor:

    systemctl restart tor

A bug report should be filed after gathering more data by setting the
`ExtendedErrors` flags on the `SocksPort`, which give an error code
that can be looked up in the `torrc` manual page.

For sites hosted behind onionbalance, however, the issue might lie
elsewhere, see [tpo/onion-services/onionbalance#9](https://gitlab.torproject.org/tpo/onion-services/onionbalance/-/issues/9).

## Disaster recovery

<!-- what to do if all goes to hell. e.g. restore from backups? -->
<!-- rebuild from scratch? not necessarily those procedures (e.g. see -->
<!-- "Installation" below but some pointers. -->

# Reference

<!-- this section is a more in-depth review of how this service works, -->
<!-- how it's setup. day-to-day operation should be covered in -->
<!-- tutorial or how-to, this is more in-depth -->

<!-- a good guide to "audit" an existing project's design: -->
<!-- https://bluesock.org/~willkg/blog/dev/auditing_projects.html -->
<!-- the following sections are partially based on that -->

## Installation

<!-- how to setup the service from scratch -->

## Upgrades

<!-- how upgrades are performed. preferably automated through Debian -->
<!-- packages, otherwise document how upgrades are performed. see also -->
<!-- the Testing section below -->

## SLA

<!-- this describes an acceptable level of service for this service -->

## Design and architecture

<!-- how this is built -->
<!-- should reuse and expand on the "proposed solution" discussed in -->
<!-- a previous RFC or the Discussion section below. it's a -->
<!-- "as-built" documented, whereas the "Proposed solution" is an -->
<!-- "architectural" document, which the final result might differ -->
<!-- from, sometimes significantly -->

## Services

<!-- open ports, daemons, cron jobs -->

## Storage

<!-- databases? plain text file? the frigging blockchain? memory? -->

## Queues

<!-- email queues, job queues, schedulers -->

## Interfaces

<!-- e.g. web APIs, commandline clients, etc -->

## Authentication

<!-- SSH? LDAP? standalone? -->

## Implementation

<!-- programming languages, frameworks, versions, license -->

## Related services

<!-- dependent services (e.g. authenticates against LDAP, or requires -->
<!-- git pushes)  -->

## Issues

<!-- such projects are never over. add a pointer to well-known issues -->
<!-- and show how to report problems. usually a link to the -->
<!-- issue tracker. consider creating a new Label to regroup the -->
<!-- issues if using the general tracker. see also TPA-RFC-19. -->

There is no issue tracker specifically for this project, [File][] or
[search][] for issues in the [team issue tracker][search] with the
label ~Foo.

 [File]: https://gitlab.torproject.org/tpo/tpa/team/-/issues/new
 [search]: https://gitlab.torproject.org/tpo/tpa/team/-/issues?label_name%5B%5D=Foo

## Maintainer

<!-- document who deployed and operates this service, the team and -->
<!-- ideally the person inside that team -->

## Users

<!-- who the main users are, how they use the service. possibly reuse -->
<!-- the Personas section in the RFC, if available. -->

## Upstream

<!-- who the upstreams are, if they are still active, -->
<!-- collaborative, support channels, see -->
<!-- also the "Issues" and "Upgrades" section above -->

## Monitoring and metrics

<!-- describe how this service is monitored -->
<!-- what kind of performance metrics do we keep, where and for how lng -->

## Tests

<!-- how the service can be tested, for example after major changes -->
<!-- like IP address changes or upgrades. describe CI, test suites, linting -->

## Logs

<!-- where are the logs? how long are they kept? any PII? -->

## Backups

<!-- does this service need anything special in terms of backups? -->
<!-- e.g. locking a database? special recovery procedures? -->

## Other documentation

<!-- references to upstream documentation, if relevant -->

# Discussion

<!-- the "discussion" section is where you put any longer conversation -->
<!-- about the project that you will not need in a casual -->
<!-- review. history of the project, why it was done the way it was -->
<!-- (as opposed to how), alternatives, and other proposals are -->
<!-- relevant here. -->

<!-- this at least partly overlaps with the TPA-RFC process (see -->
<!-- policy.md), but in general should defer to proposals when -->
<!-- available -->

## Overview

<!-- describe the overall project. should include a link to a ticket -->
<!-- that has a launch checklist -->

<!-- if this is an old project being documented, summarize the known -->
<!-- issues with the project. -->

## Security and risk assessment

<!--

 5. When was the last security review done on the project? What was
    the outcome? Are there any security issues currently? Should it
    have another security review?

 6. When was the last risk assessment done? Something that would cover
    risks from the data stored, the access required, etc.

-->

## Technical debt and next steps

<!--

 7. Are there any in-progress projects? Technical debt cleanup?
    Migrations? What state are they in? What's the urgency? What's the
    next steps?

 8. What urgent things need to be done on this project?

-->

## Proposed Solution

<!-- Link to RFC -->

## Other alternatives

<!-- include benchmarks and procedure if relevant -->
