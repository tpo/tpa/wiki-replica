# Rebooting Dragon

Dragon runs the Jenkins orchestrator and a number of Jenkins Agents and thus
rebooting it may cause inconveniences to developers in case thery're working on
a release (check the release calendar) or even just waiting for job results.

## When to avoid rebooting

- During the weekend before a release (starting friday), as jobs that update
  Tor Browser are automatically run in that period
  (`*-tor-browser-*+force-all-tests` jobs).

- During the 2 days a release takes (this could screw up the whole RM's
  schedule for these 2 days).

- Until a couple of days after a release, as lots of users might be upgrading
  during this period.

## Reboot steps

1. Make sure it is not an inconvenient moment to reboot Jenkins VM and agents
   (see "Restarting Jenkins" below if you're unsure).

2. Announce it in XMPP 15 minutes in advance.

3. Take note of which jobs are running in Jenkins.

4. Reboot (see `notes.mdwn` for info about how to unlock LUKS).

5. Once everything is back up, reschedule the aborted Jenkins jobs, replacing
   any `test_Tails_*` by their `build_Tails_*` counterparts. (Rationale: this
   is simpler than coming up with the parameters needed to correctly start the
   `test_Tails_*` jobs).
