# Boot times

The times below assume there's no VM running prior to execution of the reboot
command:

```
sudo virsh list --name | xargs -l sudo virsh shutdown
```

## Physical servers

- Chameleon: 2m
- Dragon: 1m
- Iguana: 1m
- Lizard: 3m
- Skink: 3m25s
- Stone: 1m
