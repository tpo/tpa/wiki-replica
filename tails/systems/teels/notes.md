# Information

* `teels` is a VM hosted by PUSCII
* It's going to run our secondary DNS

# SSH

 - hidden service: rx4qeeco7vx7jxqk.onion
 - SHA256:rrATheUrJTEPg1JN+CvTLzsL1dwIxE3I2/jutVxQbl4 (DSA)
 - SHA256:hsD++jnCu9/+LD6Dp0X7W3hJzcbYRuhBrc5LV34Dgws (ECDSA)
 - SHA256:C2cuuIFff1IWqeLY84k2iFJI8FdaUxTbQIxBZk90smw (ED25519)
 - SHA256:4ninUlXylJUGa1oXBT0sMuu1S9x+zjGTbgNinOa0DI0 (RSA)

# Kernel

The kernel is pinned by the hypervisor and has no module support,
if you need a kernel upgrade or new features, contact admin@puscii.nl
or ask in #puscii on irc.indymedia.org.

# Rebooting

Nothing special is needed (storage encryption is done on the
virtualization host).
