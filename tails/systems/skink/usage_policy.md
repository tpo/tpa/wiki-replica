Usage policy for skink.tails.net
=====================================

- This is a server for development and testing. This means it might break at
  anytime. 

- Avoid as much as possible touching VMs (and related firewall configs) that
  someone else created, and remember to cleanup once you're not using them
  anymore.

- Warn other sysadmins as soon as possible when issues happen that can impact
  their work. (eg. if the host becomes inaccessible, or if you deleted the
  wrong VM by mistake)

- Always have in mind that there are currently no backups in place for skink.

- The server is currently configured by our production Puppet Server setup,
  because we want to have the host itself as stable as possible to avoid, for
  example, losing access.

- Adding/configuring VMs might need modifying the host's firewall config. We'll
  try ad-hoc deliberation for configuration changes to VM and firewall, trying
  to keep it as simples as possible, and try to use Puppet only when that makes
  our work easier and simpler. Make sure you have a copy of any custom config
  you might need in case it gets overwritten.
