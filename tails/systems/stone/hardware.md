* motherboard: PC Engines APU2C4 PRT90010A
* chassis: Supermicro CSE-512L-260B 14" Mini 1U 260W
* drives:
  - WD80PUZX 8000GB SATA III
  - Seagate NAS 3.5" 8TB ST8000NE0004 Ironwolf Pro
  - HGST Ultrastar He10 8TB SATA III
* PCIe SATA controller: DeLOCK 95233
