# Periodic tasks

:warning: This process should be merge with TPA's process at some point during
the [Tails/Tor merge
process](/tpo/tpa/team/-/wikis/policy/tpa-rfc-73-tails-infra-merge-roadmap).

The following tasks have to be manually performed by sysadmins on a priodic
basis

- Jenkins upgrade: quarterly
- Mirrors processing: react to incoming requests in the sysadmins list
- SPAM training: email is deleted after 3 days in the SPAM mailbox
