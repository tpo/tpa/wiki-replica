# GitLab Runners

:warning: This service will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

[[_TOC_]]

## Overview

Our GitLab Runners are configured via Puppet[1][] and run in VMs in our CI servers:

- `gitlab-runner.iguana` (Currently disabled because of performance issues[2][])
- `gitlab-runner2.dragon`

## Security concerns

The software stack we currently use for GitLab Runners is:

- Debian Libvirt host on top of physical hardware
- Debian VM
- GitLab Runner
- Docker
- Container images from `registry.gitlab.tails.boum.org`
- Containers

Because we give our GitLab Runners the privilege to push to protected branches
of tails/tails> we have several security concerns:

- Access Tokens: these should be "Protected" (i.e. only be made available to
  jobs run for protected branches) to make sure only users already authorized
  to modify those branches can have access to such powerful tokens.

- The GitLab Runner binaries should be cryptographically verified and
  automatically upgraded. We currently install them from Debian repositories,
  but we may need to switch to upstream repositories in the near future depending
  on whether the one in Debian is upgraded by the time GitLab 18 is released[3][].

- Having a trust path to container images: This is currently achieved by
  building our own container images and restricting our Runners to only use
  images from our own registry.

- There are currently some non-human users with Maintainer privileges that can push to
  protected branches in tails/tails>:

  - `@role-update-website`: used to automatically push IkiWiki updates made via
    GitLab CI back to the repo.

  - `@role-weblate-gatekeeper`: used to filter pushes from Weblate integration;
    has hooks in place (in Gitolite) to only allow pushes that modify `.po`
    files only.

  - `@role-branch-merger`: used to create MRs via GitLab Scheduled Pipelines to
    automatically merge certain branches of the main Tails repo into one
    another.

  Special care is needed with such users to mitigate potential attacks
  (protection of Access Tokens as described above, careful ACL configuration in
  GitLab projects, maybe extra mitigations as in the case of Weblate, etc).

[1]: https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/gitlab_runner.pp
[2]: https://gitlab.tails.boum.org/tails-sysadmin/-/issues/18086
[3]: https://docs.gitlab.com/runner/register/#register-with-a-runner-registration-token-deprecated
