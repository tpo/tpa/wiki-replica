# Website builds and deployments

:warning: This process will become outdated with tpo/tpa/team#41947 and this
page should then be updated.

Since June 2024 our website is built in GitLab CI an deployed from there to all
mirrors[1][].

Currently, some manual steps are needed so the machinery works:

- For building, a project access token must be manually created and configured
  as a CI environment variable.
- For deploying, the SSH known hosts and private key data must be manually
  configures as CI environment variables.

See below details on how to do those.

## Manual configuration of a project access token for website builds

The website is built by Ikiwiki (see `.gitlab-ci.yml` in tails/tails>[2][])
which, in the case of `build-website` jobs run for the `master` branch, is
given the `--rcs git` option, which causes IkiWiki to automatically commit
updates to `.po` files and push them back to `origin`.

Because job tokens aren't allowed to push to the repository[3][], we instead use
a project access token[4][] with limited role and scope. In order for that to
work, the git-remote URL must have basic auth credentials: any value can be
used for user and the project access token must be set as the password. We
currently use an environment variable called `$PROJECT_TOKEN_REPOSITORY_RW` to
make that possible.

In order to configure that environment variable:

1. Login as root in GitLab
2. Impersonate the `role-update-website` user[5][]
3. Create a Personal Access Token for the `role-update-website` user[6][] with:
   - Token name: `WEBSITE_BUILD_PROJECT_ACCESS_TOKEN`
   - Expiration date: 1 year from now
   - Select scope: `write_repository`
4. Add a CI/CD variable to the tails/tails> project[7][] with:
   - Type: Variable
   - Environments: All
   - Visibility: Masked
   - Flags: Protect variable
   - Description: Project Access Token with repository RW scope
   - Key: `WEBSITE_BUILD_PROJECT_ACCESS_TOKEN`
   - Value: The token created above.

## Manual configuration of SSH credentials for website deployments

Once the website is built in the CI, the resulting static data is saved as an
artifact and passed on to the next stage, which handles deployment.

Deployment is done via SSH to all mirrors and in order for that to work two
environment variables must be set for the deploy jobs:

- SSH known hosts file:
  - Type: File
  - Environments: All
  - Visibility: Visible
  - Flags: Protect variable
  - Key: `WEBSITE_DEPLOY_SSH_KNOWN_HOSTS`
  - Value: The output of `ssh-keyscan` for all mirrors to which the website is
    deployed.
- SSH private key file:
  - Type: File
  - Environments: production
  - Visibility: Visible
  - Flags: Protect variable
  - Key: `WEBSITE_DEPLOY_SSH_PRIVATE_KEY`
  - Value: The content of the private SSH key created by the
    `tails::profile::website` Puppet class, which can be found in the SSH
    "keymaster" (currently `puppet.lizard`) at
    `/var/lib/puppet-sshkeys/tails::profile::website/key`.

[1]: https://gitlab.tails.boum.org/tails-sysadmin/-/issues/17364
[2]: https://gitlab.tails.boum.org/tails/tails/-/blob/master/.gitlab-ci.yml
[3]: https://docs.gitlab.com/ee/ci/jobs/ci_job_token.html#to-git-clone-a-private-projects-repository
[4]: https://docs.gitlab.com/ee/user/project/settings/project_access_tokens.html
[5]: https://gitlab.tails.boum.org/admin/users/role-update-website
[6]: https://gitlab.tails.boum.org/-/user_settings/personal_access_tokens
[7]: https://gitlab.tails.boum.org/tails/tails/-/settings/ci_cd
