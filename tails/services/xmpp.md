XMPP
====

:warning: This service will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

We're currently using several rooms at the chat.disroot.org MUC:

- tails-assembly@chat.disroot.org
- tails-bar@chat.disroot.org
- tails-board@chat.disroot.org
- tails-dev@chat.disroot.org
- tails-sysadmins@chat.disroot.org
- tails@chat.disroot.org

Important:

- Only disroot accounts are able to configure some important room parameters,
  such as persistency.
- We use the `tailsbot@chat.disroot.org` as an owner of those rooms in order to
  configure their persistency properly. Creds can be found in:
  [[service/password-store]]
