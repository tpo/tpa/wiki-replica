# Decommission

:warning: This process will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

## Initial setup

Set some environment variables on your own system:

Set this variable to what "facter fqdn" would return on the system
which is being decommissioned; it's also the name of the Puppet node:

    FQDN=XXX

Set this variable to what "facter hostname" would return on the system
which is being decommissioned:

    HOSTNAME=XXX

Set this variable to what "facter ipaddress" would return on the system
which is being decommissioned:

    IP=XXX

## Steps

- Delete Puppet configuration in `puppet-code.git`:

   - the section dedicated to the decommissioned node in `nodes.pp`
   - `hieradata/node/$FQDN.yaml` and `hieradata/node/$FQDN.eyaml`
   - all references in `hierdata/common.yaml`
   - Look for other configuration:
       - `git grep --recurse-submodules -F "$FQDN"`
       - `git grep --recurse-submodules -F "$HOSTNAME"`
       - Look for files named `*"$HOSTNAME"*`

- Clean up certificates, cached facts, and reports on the Puppet master:

        ssh puppet.lizard sudo puppet node deactivate "${FQDN:?}"
        ssh puppet.lizard sudo puppet node clean "${FQDN:?}"

- Wipe sensitive data

  Depending on whether we control the hardware or not,
  on whether the underlying disks are SSDs or not,
  this may or not may not be feasible & worth it.

- Consider deleting backups

  If you decide to delete backups, see "Deleting backups of a decommissioned
  system" in [[tails/services/backups]].

- Delete the VM definition in libvirt.

  For example, for a VM hosted on lizard, run:

        ssh lizard.tails.net virsh undefine "${HOSTNAME:?}"

- Delete the storage volumes formerly used by this VM.
