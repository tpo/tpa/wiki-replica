# a n00b's guide to tails infra

or the tale of groente's travels in team-sysadmin wonderland...

[[_TOC_]]

## get the right repo!

1. git.tails.net is only accessible by sysadmins:
   * hosts authoritative repositories for Puppet modules.
   * is hosted in a VM in lizard.
   * use these if you want to clone and push outside of the manifests repo's submodule tree.
2. gitlab.tails.net:
   * is hosted by immerda
   * SSH fingerprint can be found at: https://tails.net/contribute/working_together/GitLab
   * puppet- repos hosted there are mirrors of the ones hosted in git.tails.net and manual changes will be overridden.

make sure you pull your stuff from git.tails.net, don't push to the git repos on GitLab, anything you push there will be overwritten!

this page might help, or not: https://tails.net/contribute/git/

## fixing stuff on lizard with puppet

so you found something wrong on lizard or one of it's vm's and want to change a configuration file somewhere? don't even think about opening vi on the server... fix it in puppet!

but first, create an issue in GitLab :)

then, find out which repo you need and clone that repo from git.tails.net into a local working dir. make a branch named after the GitLab issue, do your thing, commit & push. then ask for review on GitLab.

!! a little bit here on how to test your stuff would be really cool... !!

once your Git branch has passed review, you're good to go! go to your local working dir, checkout master, merge, delete your old branch & push!

but that's not all... you also need to update puppet-code. cd into there, cd into the submodule you've been working with and git pull origin master. then cd ../.. back to puppet-code and run git status, you should see your the directory of your submodule in the modifided list. git add modules/yoursubmodule, git commit, git push, and wait for the puppet magic to commence!
