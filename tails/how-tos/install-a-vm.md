# Installing a VM

:warning: This process will change during
[[policy/tpa-rfc-73-tails-infra-merge-roadmap]] and this page should be updated
when that happens.

1. Copy the install-vm.sh script to the hypervisor. 

2. Run ./install-vm.sh [-d disksize] [-v vcpu] [-r ram] -n hostname -i ip. This script starts by outputting the root password, be sure to copy that.

3. In puppet-hiera-node, create a file called <fqdn>.yaml and add an entry for tails::profile::network::interfaces.

4. In puppet-code, update the hieradata/node submodule and add a node definition in manifest/nodes.pp

5. Once the install is done, log in on the console as root and run puppet agent -t.

6. Log in to the puppetmaster and run ``puppet ca sign <fqdn>``.

7. Go back to the node you're installing and run ``puppet agent -t`` several times. Then, reboot the machine.

8. Add the SSH onion address (``cat /var/lib/tor/ssh-hidden-v3/hostname``) to onions.mdwn in this repo, as well as the appropriate file under Machines/Servers in summit.wiki.

9. Add the SSH fingerprints (``cd /etc/ssh;for i in `ls *pub`;do ssh-keygen -l -f $i;done``) to the appropriate file under Machines/Servers in summit.wiki.

10. Add the root password to our pass repository.

11. Wait for all the other nodes to collect the exported resources from the new node (this should be done within half an hour) and you're done!
