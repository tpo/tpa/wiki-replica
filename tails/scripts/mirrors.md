Managing mirrors
================

Mirrors are now managed directly via Puppet. See:

- the [tails::profile::mirrors_json](https://gitlab.tails.boum.org/tails/puppet-tails/-/blob/master/manifests/profile/mirrors_json.pp?ref_type=heads) defined resource.
- the [Mirror pools](https://tails.net/contribute/design/mirrors/) documentation.

Scripts
=======

dns-pool
--------

Dependencies:

    sudo apt install \
       python3-dns

geoip
-----

Dependencies:

    sudo apt install \
       geoip-database-extra \
       python3-geoip

stats
-----

This script depends on the `geoip` one (see above).
