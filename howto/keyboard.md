# Dealing with messed up consoles

For various reasons, it's possible that, during a rescue operation,
you end up on a virtual console that has a keymap set differently than
what you might expect.

For excellent and logical historical reasons, different countries have
[different keyboard layouts](https://en.wikipedia.org/wiki/Keyboard_layout#Latin-script_keyboard_layouts) and while that's usually not a problem
on daily operations using SSH, when you hit a serial console, the
remote configuration actually takes effect.

This will manifest itself as you failing to enter the root password on
a console, for example. This is especially present on some hosts
configured with a German keyboard layout ([QWERTZ](https://en.wikipedia.org/wiki/QWERTZ)), or inversely,
if you're used to such a keyboard (or the french [AZERTY](https://en.wikipedia.org/wiki/AZERTY) layout),
most hosts configured with the english [QWERTY](https://en.wikipedia.org/wiki/QWERTY) layout.

A few tips, for QWERTY users landing on a QWERTZ layout:

 * <kbd>Y</kbd> and <kbd>Z</kbd> are reversed, otherwise most letters
   are in the same place

 * <kbd>-</kbd> (dash) is left of the right shift key, i.e. in place
   of <kbd>/</kbd> (slash)

 * <kbd>/</kbd> (slash) is above <kbd>7</kbd> (so
   <kbd>shift-seven</kbd>)

# Resetting a system to a US keyboard

Most systems should generally have a US layout, but if you find a
system with a German keyboard layout, you can reset it with the
following procedure:

    dpkg-reconfigure keyboard-configuration
    setupcon -k -f

See also the [Debian wiki Keyboard page](https://wiki.debian.org/Keyboard).
