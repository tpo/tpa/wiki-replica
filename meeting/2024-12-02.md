# Roll call: who's there and emergencies

anarcat, groente, lavamind, lelutin, zen

# Dashboard review

We did our normal weekly checkin.

# Last minute December coordination

We're going to prioritize the converging the email stuff, ganeti and
puppet upgrades, and security policy, although that might get delayed
to 2025.

# Holidays planning

Confirmed shifts discussed in the 1:1s

# 2025 roadmap validation

No major change, pauli upgraded before 2025, and anarcat will
unsubscribe from Tails nagios notifications.

# Metrics of the month

 * hosts in Puppet: 90, LDAP: 90, Prometheus exporters: 505
 * number of Apache servers monitored: 33, hits per second: 669
 * number of self-hosted nameservers: 6, mail servers: 11
 * pending upgrades: 20, reboots: 0
 * average load: 1.02, memory available: 3.73 TiB/4.99 TiB, running processes: 380
 * disk free/total: 65.44 TiB/139.91 TiB
 * bytes sent: 395.69 MB/s, received: 248.31 MB/s
 * planned bookworm upgrades completion date: 2024-10-23
 * [GitLab tickets][]: 257 tickets including...
   * open: 0
   * icebox: 157
   * future: 39
   * needs information: 10
   * backlog: 21
   * next: 12
   * doing: 11
   * needs review: 8
   * (closed: 3804)
    
 [Gitlab tickets]: https://gitlab.torproject.org/tpo/tpa/team/-/boards

Obviously, the completion date is incorrect here, as it's in the
past. As mentioned above, we're hoping to complete the bookworm
upgrade before 2025.

Upgrade prediction graph lives at:

https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/bookworm/

Note that the all-time graph was updated to be more readable, see the
gorgeous result in:

https://gitlab.torproject.org/tpo/tpa/team/-/wikis/howto/upgrades/#all-time-version-graph
